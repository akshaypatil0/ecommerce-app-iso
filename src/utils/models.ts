export interface Product{
    id: number;
    title: string
    description: string;
    image: string;
    price: number;
    category: string;
}

export interface CartItem{
    product: Product;
    quantity: number;
}

export type Category = string