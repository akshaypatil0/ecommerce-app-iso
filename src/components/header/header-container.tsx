import React, { FC } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../store/types";
import { CartState } from "../../store/reducers/cart/types";
import Header from "./header";

const HeaderContainer: FC = () => {
  const cart = useSelector<RootState, CartState>((state) => state.cart);

  return <Header cartCount={cart.length} />;
};

export default HeaderContainer;
