import React, { FC } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import classes from "./header.module.scss";
import L10n from "../l10n/l10n";

const Header: FC<Props> = ({ cartCount }) => {
  const router = useRouter();

  return (
    <header className={classes.header}>
      <nav>
        <Link href="/">
          <a className={router.pathname === "/" ? classes.active : ""}>
            <L10n literal="home" />{" "}
          </a>
        </Link>
        <Link href="/cart">
          <a className={router.pathname === "/cart" ? classes.active : ""}>
            <L10n literal="cart" />
            {cartCount > 0 && (
              <span className={classes.cartCount}>{cartCount}</span>
            )}
          </a>
        </Link>
        <Link href="/contact">
          <a className={router.pathname === "/contact" ? classes.active : ""}>
            <L10n literal="contact" />
          </a>
        </Link>
      </nav>
    </header>
  );
};

interface Props {
  cartCount: number;
}

export default Header;
