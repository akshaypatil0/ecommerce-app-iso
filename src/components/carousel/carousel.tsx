import React, { FC, UIEventHandler } from "react";
import classes from "./carousel.module.scss";

const Carousel: FC<Props> = ({
  tiles,
  carouselRef,
  handleScroll,
  previous,
  next,
}) => {
  return (
    <div className={classes.carouselContainer}>
      <button className={classes.previous} onClick={previous}>
        {"<"}
      </button>
      <div
        className={classes.carousel}
        ref={carouselRef}
        onScroll={handleScroll}
      >
        {tiles}
      </div>
      <button className={classes.next} onClick={next}>
        {">"}
      </button>
    </div>
  );
};

interface Props {
  tiles: JSX.Element;
  carouselRef: any;
  handleScroll: UIEventHandler<HTMLDivElement>;
  previous: () => void;
  next: () => void;
}

export default Carousel;
