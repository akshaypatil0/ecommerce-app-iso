import React, { FC } from "react";
import classes from "./carousel-tile.module.scss";

const CarouselTile: FC<Props> = ({ children, selected }) => {
  return (
    <div
      className={`${classes.carouselTile} ${selected ? classes.active : ""}`}
    >
      {children}
    </div>
  );
};

interface Props {
  selected: boolean;
}

export default CarouselTile;
