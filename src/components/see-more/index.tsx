import React, { FC } from "react";
import Link from "next/link";
import classes from "./see-more.module.scss";
import L10n from "../l10n/l10n";

const SeeMore: FC<Props> = ({ link, title }) => {
  return (
    <div className={classes.seeMore}>
      <Link href={link}>
        <a>
          <L10n literal="see_more" cue={title} />
        </a>
      </Link>
    </div>
  );
};

interface Props {
  link: string;
  title?: string;
}

export default SeeMore;
