import React, { FC } from "react";
import { useDispatch } from "react-redux";
import Link from "next/link";
import Image from "next/image";
import { Product } from "../../utils/models";
import { changeQuantity, removeFromCart } from "../../store/reducers/cart";
import classes from "./cart-list-item.module.scss";
import L10n from "../l10n/l10n";

const CartListItem: FC<Props> = ({ product, quantity }) => {
  const dispatch = useDispatch();
  function increment() {
    dispatch(changeQuantity(product.id, quantity + 1));
  }

  function decrement() {
    dispatch(changeQuantity(product.id, quantity - 1));
  }

  return (
    <div className={classes.item}>
      <Image src={product.image} alt={product.title} height={150} width={150} />
      <div className={classes.info}>
        <Link href={`/products/${product.id}`} passHref>
          <h5>{product.title}</h5>
        </Link>
        <h6>
          <L10n price={product.price} />
        </h6>
        <div className={classes.quantity}>
          <button onClick={decrement} disabled={quantity < 2}>
            -
          </button>
          <span>{quantity}</span>
          <button onClick={increment}>+</button>
        </div>
        <div className={classes.footer}>
          <div className={classes.itemTotal}>
            <h5>
              <L10n literal="total" />:
            </h5>
            <h6>
              <L10n price={product.price * quantity} />
            </h6>
          </div>
          <button onClick={() => dispatch(removeFromCart(product.id))}>
            <L10n literal="remove_from_cart" />
          </button>
        </div>
      </div>
    </div>
  );
};

interface Props {
  product: Product;
  quantity: number;
}

export default CartListItem;
