import React, { FC } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store/types";
import { setError } from "../../store/reducers/error";
import { ErrorState } from "../../store/reducers/error/types";
import DisplayError from "./display-error";

const DisplayErrorContainer: FC = () => {
  const error = useSelector<RootState, ErrorState>((state) => state.error);

  const dispatch = useDispatch();
  function handleClose() {
    dispatch(setError(""));
  }

  return <DisplayError error={error} handleClose={handleClose} />;
};

export default DisplayErrorContainer;
