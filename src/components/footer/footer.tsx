import { useRouter } from "next/router";
import React, { FC } from "react";
import classes from "./footer.module.scss";

const Footer: FC = () => {
  const router = useRouter();

  const handleLocaleChange = (e) => {
    router.push("", "", { locale: e.target.value });
  };

  return (
    <footer className={classes.footer}>
      <div className={classes.lang}>
        <p>Select language: </p>
        <select value={router.locale} onChange={handleLocaleChange}>
          <option value="en">English</option>
          <option value="en-in">English(india)</option>
          <option value="hi">हिंदी</option>
        </select>
      </div>
      <div className={classes.nav}>
        <nav>
          <a href="https://facebook.com" target="_">
            Facebook
          </a>
          <a href="https://instagram.com" target="_">
            Instagram
          </a>
          <a href="https://twitter.com" target="_">
            Twitter
          </a>
        </nav>
      </div>
    </footer>
  );
};

export default Footer;
