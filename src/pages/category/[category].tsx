import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../store/types";
import { fetchProductsByCategory } from "../../store/reducers/categories";
import { CategoriesState } from "../../store/reducers/categories/types";
import { NextPage } from "next";
import { wrapper } from "../../store";
import { ProductGrid } from "../../components";
import { Category } from "../../utils/models";
import { ThunkDispatch } from "redux-thunk";
import { ApiCallRequestAction } from "../../store/reducers/api/types";

const CategoryPage: NextPage<Props> = ({ category }) => {
  const { productsByCategory } = useSelector<RootState, CategoriesState>(
    (state) => state.categories
  );

  return (
    <div>
      <ProductGrid products={productsByCategory[category].list} />
    </div>
  );
};

CategoryPage.getInitialProps = wrapper.getInitialPageProps<Props>(
  (store) =>
    ({ query }) =>
      new Promise<Props>(async (resolve, reject) => {
        const dispatch = store.dispatch as ThunkDispatch<
          RootState,
          {},
          ApiCallRequestAction
        >;
        const category = query.category as string;
        await dispatch(fetchProductsByCategory({ category, force: true }));
        resolve({ category });
      })
);

interface Props {
  category: Category;
}
export default CategoryPage;
