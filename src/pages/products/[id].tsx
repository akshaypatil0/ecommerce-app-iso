import { NextPage } from "next";
import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../store/types";
import { fetchProduct } from "../../store/reducers/product";
import { ProductState } from "../../store/reducers/product/types";

import { AddToCartButton, LoadingSpinner } from "../../components";
import { wrapper } from "../../store";
import { ThunkDispatch } from "redux-thunk";
import { ApiCallRequestAction } from "../../store/reducers/api/types";

import Image from "next/image";

import classes from "../../styles/product.module.scss";
import Head from "../../components/head/head";

const Product: NextPage = () => {
  const { data: product, isFetching: isLoading } = useSelector<
    RootState,
    ProductState
  >((state) => state.product);

  if (isLoading) {
    return (
      <div className={classes.product}>
        <Head page="loading..." />
        <LoadingSpinner />;
      </div>
    );
  }

  return (
    <div className={classes.product}>
      <Head page={product.title} description={product.description} />
      <div className={classes.img}>
        <Image
          src={product.image}
          alt={product.title}
          layout="fill"
          objectFit="contain"
        />
      </div>
      <div className={classes.info}>
        <h5>{product.title}</h5>
        <p>{product.description}</p>
        <h6>$ {product.price}</h6>
        <div className={classes.delivery}>
          <h6>Delivery Information</h6>
          <li>Get it within 2 days</li>
          <li>Pay on delivery available</li>
          <li>Easy 30 days return & exchange available</li>
        </div>
        <AddToCartButton product={product} />
      </div>
    </div>
  );
};

Product.getInitialProps = wrapper.getInitialPageProps(
  (store) =>
    async ({ query }) => {
      const dispatch = store.dispatch as ThunkDispatch<
        RootState,
        {},
        ApiCallRequestAction
      >;

      await dispatch(fetchProduct(query.id));
    }
);

export default Product;
