import { NextPage } from "next";
import React from "react";
import { useSelector } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import {
  Carousel,
  LoadingSpinner,
  ProductGrid,
  ProductTile,
  SeeMore,
} from "../components";
import Head from "../components/head/head";
import L10n from "../components/l10n/l10n";
import { wrapper } from "../store";
import { ApiCallRequestAction } from "../store/reducers/api/types";
import { fetchAllCategories } from "../store/reducers/categories";
import { CategoriesState } from "../store/reducers/categories/types";
import { fetchProducts } from "../store/reducers/products";
import { ProductsState } from "../store/reducers/products/types";
import { RootState } from "../store/types";
import classes from "../styles/home.module.scss";

const Home: NextPage = () => {
  const { list: products, isFetching } = useSelector<RootState, ProductsState>(
    (state) => state.products
  );

  const categories = useSelector<RootState, CategoriesState["list"]>(
    (state) => state.categories.list
  );
  const productsByCategory = useSelector<
    RootState,
    CategoriesState["productsByCategory"]
  >((state) => state.categories.productsByCategory);

  return (
    <div className={classes.home}>
      <Head page="Home" />
      <section>
        <div className={classes.heading}>
          <h1>
            <L10n literal="featured_products" />
          </h1>
        </div>
        <ProductGrid
          products={products}
          isLoading={isFetching}
          limit={5}
          renderSeeMore={() => <SeeMore link="/products" />}
        />
      </section>
      {categories.map((category) => (
        <section key={category}>
          <div className={classes.heading}>
            <h1>
              <L10n literal="explore" cue={category} />
            </h1>
          </div>
          {productsByCategory[category] &&
          !productsByCategory[category].isFetching ? (
            <Carousel
              items={productsByCategory[category].list.map((product) => (
                <ProductTile product={product} key={product.id} />
              ))}
              renderSeeMore={() => (
                <SeeMore title={category} link={`/category/${category}`} />
              )}
            />
          ) : (
            <LoadingSpinner />
          )}
        </section>
      ))}
    </div>
  );
};

export default Home;

Home.getInitialProps = wrapper.getInitialPageProps((store) => async () => {
  const dispatch = store.dispatch as ThunkDispatch<
    RootState,
    {},
    ApiCallRequestAction
  >;

  await Promise.all([
    dispatch(fetchProducts({ limit: 5 })),
    dispatch(fetchAllCategories({ limit: 4 })),
  ]);
});
