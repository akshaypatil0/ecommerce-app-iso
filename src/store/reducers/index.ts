import { combineReducers } from "redux";
import apiReducer from "./api";
import productsReducer from "./products";
import productReducer from "./product";
import categoriesReducer from "./categories";
import cartReducer from "./cart";
import errorReducer from "./error";
import { RootState } from "../types";
import { HYDRATE } from "next-redux-wrapper";

const combinedReducer = combineReducers<RootState>({
  api: apiReducer,
  products: productsReducer,
  product: productReducer,
  categories: categoriesReducer,
  cart: cartReducer,
  error: errorReducer,
});

const rootReducer = (state, action) => {
  if (action.type === HYDRATE) {
    return {
      ...state,
      ...action.payload,
    };
  }
  return combinedReducer(state, action);
};

export default rootReducer;
