import { AnyAction } from "redux";

// Action types
export interface SetErrorAction extends AnyAction {
  type: "set-error";
  payload: string;
}

export type ErrorActions = SetErrorAction;

//reducer
export type ErrorState = string | null;
