import { AnyAction } from "redux";
import { Product } from "../../../utils/models";

// Action types
export interface FetchProductsRequestAction extends AnyAction {
  type: "fetch-products-request";
}
export interface FetchProductsSuccessAction extends AnyAction {
  type: "fetch-products-success";
  payload: Product[];
}
export interface FetchProductsFailedAction extends AnyAction {
  type: "fetch-products-failed";
}

export type ProductsActions =
  | FetchProductsRequestAction
  | FetchProductsSuccessAction
  | FetchProductsFailedAction;
//reducer

export type ProductsState = {
  list: Product[];
  isFetching: boolean;
  lastFetched?: number;
};
