import { AxiosError, Method } from "axios";
import { AnyAction } from "redux";
import { Actions } from "../../types";

// Action types
export interface ApiCallRequestAction<A extends AnyAction = Actions>
  extends AnyAction {
  type: "api-call-request";
  payload: {
    url: string;
    method?: Method;
    data?: any;
    onSuccess: A["type"];
    onRequest?: A["type"];
    onFailure?: A["type"];
  };
  meta: {
    lastFetched?: number;
    cacheDuration?: number;
    category?: string;
  };
}

export interface ApiCallRequestedAction extends AnyAction {
  type: "api-call-requested";
}

export interface ApiCallFailedAction extends AnyAction {
  type: "api-call-failed";
  payload: AxiosError;
}

export type ApiActions =
  | ApiCallRequestAction
  | ApiCallRequestedAction
  | ApiCallFailedAction;

// State type
export type ApiState = {
  isFecthing: boolean;
  error: string | null;
};
