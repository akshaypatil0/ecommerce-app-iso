import { AxiosError } from "axios";
import { ActionCreator, Reducer } from "redux";
import {
  ApiActions,
  ApiCallFailedAction,
  ApiCallRequestedAction,
  ApiState,
} from "./types";

// Actions creators
export const apiCallRequest: ActionCreator<ApiCallRequestedAction> = () => ({
  type: "api-call-requested",
});

export const apiCallFailed: ActionCreator<ApiCallFailedAction> = (
  error: AxiosError
) => ({
  type: "api-call-failed",
  payload: error,
});

// Reducer

const initialState: ApiState = {
  isFecthing: false,
  error: null,
};

const apiReducer: Reducer<ApiState, ApiActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case "api-call-requested":
      return {
        ...state,
        isFecthing: true,
      };
    case "api-call-failed":
      return {
        ...state,
        isFecthing: false,
        error: action.payload.message,
      };

    default:
      return state;
  }
};

export default apiReducer;
