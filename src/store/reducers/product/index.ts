import { ActionCreator, Reducer } from "redux";
import * as url from "../../../utils/urls";
import { Product } from "../../../utils/models";
import { ApiCallRequestAction } from "../api/types";
import { ProductActions, ProductState } from "./types";

// Action creators
export const fetchProduct: ActionCreator<ApiCallRequestAction<ProductActions>> =
  (id: Product["id"]) => {
    return {
      type: "api-call-request",
      payload: {
        url: url.getProductById(id),
        onRequest: "fetch-product-request",
        onSuccess: "fetch-product-success",
        onFailure: "fetch-product-failed",
      },
      meta: {},
    };
  };

//reducer
const initialState: ProductState = {
  data: null,
  isFetching: false,
};

const productReducer: Reducer<ProductState, ProductActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case "fetch-product-request":
      return {
        ...state,
        isFetching: true,
      };

    case "fetch-product-success":
      return {
        ...state,
        data: action.payload,
        isFetching: false,
      };

    case "fetch-product-failed":
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
};

export default productReducer;
