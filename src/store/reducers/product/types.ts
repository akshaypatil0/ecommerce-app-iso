import { AnyAction } from "redux";
import { Product } from "../../../utils/models";

// Action types
export interface FetchProductRequestAction extends AnyAction {
  type: "fetch-product-request";
}
export interface FetchProductSuccessAction extends AnyAction {
  type: "fetch-product-success";
  payload: Product;
}
export interface FetchProductFailedAction extends AnyAction {
  type: "fetch-product-failed";
}

export type ProductActions =
  | FetchProductRequestAction
  | FetchProductSuccessAction
  | FetchProductFailedAction;

//reducer
export type ProductState = {
  data: Product | null;
  isFetching: boolean;
};
